import { GETALLTASKS, CREATETASK, DELETETASK ,UPDATETASK } from "./tasksActionTypes";

const initailState = {
  tasks: [],
};
function tasksReducer(state = initailState, action) {
  switch (action.type) {
    case GETALLTASKS:
      return {
        tasks: [...action.tasks],
      };
    case CREATETASK:
      return {
        tasks: [...state.tasks, action.newTask],
      };
    case DELETETASK:
      return {
        tasks: state.tasks.filter((task) => task.id !== action.id),
      };
    case UPDATETASK:
      return {
        tasks: state.tasks.map((task) => {
          if (task.id === action.id) {
            task.content = action.newName;
            return task;
          } else {
            return task;
          }
        }),
      };
    default:
      return state;
  }
}

export default tasksReducer;

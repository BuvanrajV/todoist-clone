import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Projects from "./Sidebar/Projects";
import { FiInbox } from "react-icons/fi";
import { MdOutlineCalendarToday } from "react-icons/md";
import { BiCalculator } from "react-icons/bi";
import { BsFilterRight } from "react-icons/bs";
import { addProjectToApi, getProjectsFromAPi } from "../../api.mjs";
import {
  createProject,
  getAllProjects,
} from "../../redux/projects/projectsAction";
import { Flex } from "@chakra-ui/react";

class Sidebar extends Component {
  state = {
    projectName: "",
  };

  componentDidMount() {
    getProjectsFromAPi()
      .then((projects) => this.props.getProjects(projects))
      .catch((err) => console.error(err));
  }

  createProject = (event) => {
    event.preventDefault();
    addProjectToApi(this.state.projectName)
      .then((newProject) => {
        this.props.addProject(newProject);
        this.setState({ projectName: "" });
      })
      .catch((err) => console.error(err));
  };

  render() {
    return (
      this.props.projects[0] !== undefined && (
        <section className="sidebar">
          <Flex direction="column" gap="1vh" mb="3vh">
            <Flex align="center" gap="0.5vw">
              <div>
                <FiInbox color="blue" />
              </div>
              <Link
                to={{
                  pathname: `/${this.props.projects[0].id}`,
                  project: this.props.projects[0],
                }}
              >
                <div className="color-20">Inbox</div>
              </Link>
            </Flex>
            <Flex align="center" gap="0.5vw">
              <div>
                <MdOutlineCalendarToday color="green" />
              </div>
              <Link to="/">
                <div className="color-20"> Today</div>
              </Link>
            </Flex>
            <Flex align="center" gap="0.5vw">
              <div>
                <BiCalculator color="purple" />
              </div>
              <div className="color-20">Upcoming</div>
            </Flex>
            <Flex align="center" gap="0.5vw">
              <div>
                <BsFilterRight color="#eb8909" />
              </div>
              <div className="color-20">Filters & Labels</div>
            </Flex>
          </Flex>
          <Flex direction="column" gap="1vh" pr="2vw">
            <Flex justify="space-between">
              <div style={{ color: "#0000008F", fontWeight: "600" }}>
                Projects
              </div>
              <div>
                <button
                  type="button"
                  className="btn"
                  data-bs-toggle="modal"
                  data-bs-target="#staticBackdrop"
                >
                  +
                </button>

                <div
                  className="modal fade"
                  id="staticBackdrop"
                  data-bs-backdrop="static"
                  data-bs-keyboard="false"
                  tabIndex="-1"
                  aria-labelledby="staticBackdropLabel"
                  aria-hidden="true"
                >
                  <div className="modal-dialog">
                    <div className="modal-content">
                      <div className="modal-header">
                        <h1
                          className="modal-title fs-5"
                          id="staticBackdropLabel"
                        >
                          Add project
                        </h1>
                        <button
                          type="button"
                          className="btn-close"
                          data-bs-dismiss="modal"
                          aria-label="Close"
                        ></button>
                      </div>
                      <form
                        className="modal-body"
                        onSubmit={this.createProject}
                      >
                        <div>Name</div>
                        <input
                          className="border"
                          onChange={(e) =>
                            this.setState({ projectName: e.target.value })
                          }
                          value={this.state.projectName}
                        ></input>

                        <div className="modal-footer">
                          <button
                            type="button"
                            className="btn btn-secondary"
                            data-bs-dismiss="modal"
                          >
                            Close
                          </button>
                          <button
                            type="submit"
                            className="btn btn-primary"
                            data-bs-dismiss="modal"
                          >
                            Add
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </Flex>
            <div>
              <ul>
                <Projects projects={this.props.projects} />
              </ul>
            </div>
          </Flex>
        </section>
      )
    );
  }
}

const mapStateToProps = (state) => {
  return {
    projects: state.projectsReducer.projects,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addProject: (newProject) => dispatch(createProject(newProject)),
    getProjects: (projects) => dispatch(getAllProjects(projects)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);

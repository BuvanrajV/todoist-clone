import React, { Component } from "react";
import { Flex, Button } from "@chakra-ui/react";
import {
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverBody,
  PopoverArrow,
  PopoverCloseButton,
} from "@chakra-ui/react";

class Task extends Component {
  state = {
    newTaskNameToUpdate: "",
    isRenameClicked: false,
  };
  render() {
    let task = this.props.task;
    if (task.projectId === this.props.projectId) {
      return (
        <>
          <div key={task.id} className="mb-4">
            {
              this.state.isRenameClicked ? (
                <form
                  onSubmit={(event) => {
                    this.props.onUpdate(
                      event,
                      task.id,
                      this.state.newTaskNameToUpdate
                    );
                    this.setState({ isRenameClicked: false });
                  }}
                >
                  <div>
                    <input
                      placeholder="Rename"
                      onChange={(e) =>
                        this.setState({ newTaskNameToUpdate: e.target.value })
                      }
                      defaultValue={task.content}
                      style={{ border: "1px solid #202020" }}
                    ></input>
                    <button type="submit" style={{ marginLeft: "3vw" }}>
                      Rename
                    </button>
                    <button style={{ marginLeft: "1vw" }}> Cancel</button>
                  </div>
                </form>
              ) : (
                <Flex justify="space-between">
                  <div>{task.content}</div>
                  <div>
                    <Popover>
                      <PopoverTrigger>
                        <Button
                          bg="#FAFAFA"
                          marginTop="-1.5vh"
                          className="Link"
                          marginLeft="30vh"
                        >
                          ...
                        </Button>
                      </PopoverTrigger>
                      <PopoverContent>
                        <PopoverArrow />
                        <PopoverCloseButton className="Link" />
                        <PopoverBody>
                          <Flex direction="column" gap="2vh">
                            <div
                              onClick={() =>
                                this.setState({ isRenameClicked: true })
                              }
                              className="cursor"
                            >
                              Edit Task
                            </div>

                            <div
                              onClick={() => this.props.onDelete(task.id)}
                              className="cursor"
                            >
                              Delete Task
                            </div>
                          </Flex>
                        </PopoverBody>
                      </PopoverContent>
                    </Popover>
                  </div>
                </Flex>
              )
              // <button onClick={()=>this.setState({isRenameClicked : true})}>Rename</button>
            }
          </div>
        </>
      );
    }
  }
}

export default Task;

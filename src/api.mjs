import { TodoistApi } from "@doist/todoist-api-typescript";

const api = new TodoistApi("d6bbe86956c8ccec68f9a4d559265eb99d78179e");

export function getProjectsFromAPi() {
  return api.getProjects();
}

export function addProjectToApi(newProjectName) {
  return api.addProject({ name: newProjectName });
}

export function updateProjectInAPi(id, newProjectName) {
  return api.updateProject(id, { name: newProjectName });
}

export function deleteProjectInAPi(id) {
  return api.deleteProject(id);
}

export function getTasksFromAPi() {
  return api.getTasks();
}

export function addTaskToApi(id, newProject) {
  if (id === undefined) {
    return api.addTask({
      content: newProject,
      dueString: "tomorrow at 12:00",
      dueLang: "en",
      priority: 4
    });
  } else {
    return api.addTask({ content: newProject, projectId: id });
  }
}

export function updatetaskInAPi(id, newProjectName) {
  console.log(id, newProjectName);
  return api.updateTask(id, { content: newProjectName });
}

export function deleteTaskInAPi(id) {
  return api.deleteTask(id);
}

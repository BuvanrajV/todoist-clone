import {
  GETALLPROJECTS,
  CREATEPROJECT,
  DELETEPROJECT,
  UPDATEPROJECT
} from "./projectsActionType";

export const getAllProjects = (projects) => {
  return {
    type: GETALLPROJECTS,
    projects,
  };
};

export const createProject = (newProject) => {
  return {
    type: CREATEPROJECT,
    newProject,
  };
};

export const deleteProject = (id) => {
  return {
    type: DELETEPROJECT,
    id,
  };
};

export const updateProject=(id,newName)=>{
  return {
    type : UPDATEPROJECT,
    id,
    newName
  }
}
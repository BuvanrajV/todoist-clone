import React, { Component } from "react";
import Project from "./Project.jsx";

class Projects extends Component {
  render() {
    let projects = this.props.projects;
    return projects.slice(1).map((project) => {
      return (
        
          <Project key={project.id} project={project} className="Link"/>
      );
    });
  }
}

export default Projects;

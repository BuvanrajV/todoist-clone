import React from "react";
import { Flex, Input } from "@chakra-ui/react";
import { FiMenu } from "react-icons/fi";
import { SlHome } from "react-icons/sl";
import { HiArrowTrendingUp } from "react-icons/hi2";
import { BsQuestionCircle } from "react-icons/bs";
import { IoNotificationsOutline } from "react-icons/io5";

class Header extends React.Component {
  render() {
    return (
      <header className="header mb-3">
        <Flex gap="1vw">
          <div>
            <FiMenu size={20} onClick={this.props.handleSidebarComponent} className="cursor"/>
          </div>
          <div>
            <SlHome size={20} />
          </div>
          <div>
            <Input placeholder="Search" w="30vh" className="header-input" size="10" width="10vw"/>
          </div>
        </Flex>
        <Flex gap="1vw ">
          <div >Upgrade to Pro</div>
          <div>+</div>
          <Flex gap="0.5vw">
            <div className="circle">
              <HiArrowTrendingUp size={20} />
            </div>
            <div>0/5</div>
          </Flex>
          <div>
            <BsQuestionCircle size={20} />
          </div>
          <div >
            <IoNotificationsOutline size={20} />
          </div>
          <div className="circle header-profile">
            <span>B</span>
          </div>
        </Flex>
      </header>
    );
  }
}

export default Header;

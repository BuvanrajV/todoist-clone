import { BrowserRouter, Route, Switch } from "react-router-dom";
import { connect } from "react-redux";
import Sidebar from "./Content/Sidebar";
import Header from "./Header";
import { SiTodoist } from "react-icons/si";
import { getAllTasks } from "../redux/tasks/tasksActions";
import { getTasksFromAPi } from "../api.mjs";
import { CircularProgress, Flex } from "@chakra-ui/react";
import Tasks from "./Content/Tasks";

import React, { Component } from "react";
class Content extends Component {
  state = {
    isLoading: true,
    isSidebarComponent: true,
  };

  componentDidMount() {
    getTasksFromAPi()
      .then((tasks) => {
        this.props.getAllTasks(tasks);
        this.setState({ isLoading: false });
      })
      .catch((err) => console.error(err));
  }

  handleSidebarComponent = () => {
    if (this.state.isSidebarComponent) {
      this.setState({ isSidebarComponent: false });
    } else {
      this.setState({ isSidebarComponent: true });
    }
  };

  render() {
    return this.state.isLoading === false ? (
      <BrowserRouter>
        <Header handleSidebarComponent={this.handleSidebarComponent} />
        <Flex>
          {this.state.isSidebarComponent && <Sidebar />}
          <div>
            <Switch>
              <Route exact path={"/"}>
                <Tasks tasks={this.props.tasks} />
              </Route>
              <Route exact path={"/:id"} component={Tasks} />
            </Switch>
          </div>
        </Flex>
      </BrowserRouter>
    ) : (
      <Flex
        h="100vh"
        w="100vw"
        justify="center"
        align="center"
        direction="column"
        gap="3vh"
      >
        <SiTodoist color="#db4c3f" size={60} />
        <CircularProgress isIndeterminate color="#db4c3f" size={7} />
      </Flex>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    tasks: store.tasksReducer.tasks,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAllTasks: (tasks) => dispatch(getAllTasks(tasks)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Content);

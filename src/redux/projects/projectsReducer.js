import {
  GETALLPROJECTS,
  CREATEPROJECT,
  DELETEPROJECT,
  UPDATEPROJECT,
} from "./projectsActionType";

const initailState = {
  projects: [],
};

function projectsReducer(state = initailState, action) {
  switch (action.type) {
    case GETALLPROJECTS:
      return {
        projects: [...action.projects],
      };
    case CREATEPROJECT:
      return {
        projects: [...state.projects, action.newProject],
      };
    case DELETEPROJECT:
      return {
        projects: state.projects.filter((project) => project.id !== action.id),
      };
    case UPDATEPROJECT:
      return {
        projects: state.projects.map((project) => {
          if (project.id === action.id) {
            project.name = action.newName;
            return project;
          } else {
            return project;
          }
        }),
      };
    default:
      return state;
  }
}

export default projectsReducer;

import { ChakraProvider } from "@chakra-ui/react";
import { Provider } from "react-redux";
import store from "./redux/store";
import Content from "./components/Content";

function App() {
  return (
    <Provider store={store}>
      <ChakraProvider>
        <Content />
      </ChakraProvider>
    </Provider>
  );
}

export default App;

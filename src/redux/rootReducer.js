import projectsReducer from "./projects/projectsReducer";
import tasksReducer from "./tasks/tasksReducer";
import { combineReducers } from "redux";

export default combineReducers({
  projectsReducer,
  tasksReducer,
});

import { connect } from "react-redux";
import React, { Component } from "react";
import Task from "./Task.jsx";
import { deleteTaskInAPi, updatetaskInAPi, addTaskToApi } from "../../api.mjs";
import {
  deleteTask,
  updateTask,
  createTask,
} from "../../redux/tasks/tasksActions";

class Tasks extends Component {
  state = {
    taskName: "",
    isAddTaskClicked: false,
  };

  createTask = (event, id) => {
    event.preventDefault();
    addTaskToApi(id, this.state.taskName)
      .then((newTask) => {
        this.props.addTask(id, newTask);
        this.setState({ isAddTaskClicked: false });
      })
      .catch((err) => console.error(err));
  };

  deleteTask = (id) => {
    deleteTaskInAPi(id)
      .then(() => this.props.deleteTask(id))
      .catch((err) => console.error(err));
  };

  updateTask = (event, id, newTaskName) => {
    console.log("newTaskName :>> ", newTaskName);
    event.preventDefault();
    updatetaskInAPi(id, newTaskName)
      .then(() => {
        this.props.updateTask(id, newTaskName);
      })
      .catch((err) => console.error("error in updateProject", err));
  };

  render() {
    let tasks = this.props.tasks;
    return this.props.location === undefined
      ? this.props.projects[0] !== undefined && (
          <section className="tasks-section">
            <div
              style={{ fontWeight: "600", fontSize: "1.4rem" }}
              className="mb-4"
            >
              Today
            </div>
            <div className="mb-4">
              {this.state.isAddTaskClicked ? (
                <form onSubmit={(event) => this.createTask(event)}>
                  <input
                    placeholder="Create Task"
                    // defaultValue={this.state.taskName}
                    onChange={(e) =>
                      this.setState({ taskName: e.target.value })
                    }
                  ></input>

                  <button type="submit" style={{ marginLeft: "2vw" }}>
                    + Add task
                  </button>
                  <button
                    onClick={() =>
                      this.setState({ isAddTaskClicked: false, taskName: "" })
                    }
                    style={{ marginLeft: "1vw" }}
                  >
                    Cancel
                  </button>
                </form>
              ) : (
                <button
                  onClick={() => this.setState({ isAddTaskClicked: true })}
                >
                  + Add task
                </button>
              )}
            </div>
            {tasks.filter((task) => task.due !== null)[0] === undefined ? (
              <div style={{ marginLeft: "12vw", width: "15vw" }}>
                <div>
                  <img
                    src="https://d3ptyyxy2at9ui.cloudfront.net/assets/images/418012032c5aaee447289642c812e569.jpg"
                    alt="emptyProject img"
                  />
                </div>
                <div
                  style={{
                    fontWeight: "600",
                    fontSize: "1.1rem",
                    textAlign: "center",
                  }}
                >
                  {" "}
                  Start small (or dream big)...
                </div>
                <div style={{ fontSize: "0.9rem", textAlign: "center" }}>
                  Enjoy the rest of your day and don't forget to share your
                  #TodoistZero awesomeness ↓
                </div>
              </div>
            ) : (
              tasks.map((task) => {
                return (
                  task.due !== null && (
                    <Task
                      task={task}
                      key={task.id}
                      onDelete={this.deleteTask}
                      onUpdate={this.updateTask}
                      projectId={this.props.projects[0].id}
                    />
                  )
                );
              })
            )}
          </section>
        )
      : this.props.location.project !== undefined && (
          <section className="tasks-section">
            <div
              style={{ fontWeight: "600", fontSize: "1.4rem" }}
              className="mb-4"
            >
              {this.props.location.project.name}
            </div>
            <div className="mb-4">
              {this.state.isAddTaskClicked ? (
                <form
                  onSubmit={(event) =>
                    this.createTask(event, this.props.location.project.id)
                  }
                >
                  <input
                    placeholder="Create Task"
                    onChange={(e) =>
                      this.setState({ taskName: e.target.value })
                    }
                    // defaultValue={this.state.taskName}
                  ></input>

                  <button
                    type="submit"
                    style={{ marginLeft: "2vw" }}
                    className="btn"
                  >
                    + Add task
                  </button>
                  <button
                    onClick={() =>
                      this.setState({ isAddTaskClicked: false, taskName: "" })
                    }
                    style={{ marginLeft: "1vw" }}
                  >
                    Cancel
                  </button>
                </form>
              ) : (
                <button
                  onClick={() => this.setState({ isAddTaskClicked: true })}
                >
                  + Add task
                </button>
              )}
            </div>
            {tasks.filter(
              (task) => task.projectId === this.props.location.project.id
            )[0] === undefined ? (
              <div style={{ marginLeft: "12vw", width: "15vw" }}>
                <div>
                  <img
                    src="https://d3ptyyxy2at9ui.cloudfront.net/assets/images/19c6289a4432407395ea4abdfc590f52.jpg"
                    alt="emptyProject img"
                  />
                </div>
                <div
                  style={{
                    fontWeight: "600",
                    fontSize: "1.1rem",
                    textAlign: "center",
                  }}
                >
                  {" "}
                  Start small (or dream big)...
                </div>
                <div style={{ fontSize: "0.9rem", textAlign: "center" }}>
                  Track tasks, follow progress, and discuss details in one
                  central, shared project.
                </div>
              </div>
            ) : (
              tasks.map((task) => {
                return (
                  <Task
                    task={task}
                    key={task.id}
                    onDelete={this.deleteTask}
                    onUpdate={this.updateTask}
                    projectId={this.props.location.project.id}
                  />
                );
              })
            )}
          </section>
        );
  }
}
const mapStateToProps = (store) => {
  return {
    tasks: store.tasksReducer.tasks,
    projects: store.projectsReducer.projects,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addTask: (id, newTask) => dispatch(createTask(id, newTask)),
    deleteTask: (id) => dispatch(deleteTask(id)),
    updateTask: (id, newName) => dispatch(updateTask(id, newName)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Tasks);

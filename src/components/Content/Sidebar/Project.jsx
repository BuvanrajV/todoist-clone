import React, { Component } from "react";
import { Link } from "react-router-dom";

import { connect } from "react-redux";
import { Flex } from "@chakra-ui/react";
import { Modal } from "react-bootstrap";
import {
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverBody,
  PopoverArrow,
  PopoverCloseButton,
} from "@chakra-ui/react";

import { deleteProjectInAPi, updateProjectInAPi } from "../../../api.mjs";
import {
  deleteProject,
  updateProject,
} from "../../../redux/projects/projectsAction";

import { Button } from "@chakra-ui/react";

class Project extends Component {
  state = {
    newProjectName: "",
    isDeletePopup: false,
    isEditProjectPopup: false,
  };

  updateProject = (id) => {
    // event.preventDefault();
    updateProjectInAPi(id, this.state.newProjectName)
      .then(() => {
        this.props.updateProject(id, this.state.newProjectName);
        this.setState({ isEditProjectPopup: false, newProjectName: "" });
      })
      .catch((err) => console.error("error in updateProject", err));
  };

  deleteProject = (id) => {
    deleteProjectInAPi(id)
      .then(() => {
        this.props.deleteProject(id);
        this.setState({ isDeletePopup: false });
      })
      .catch((err) => console.error(err));
  };

  render() {
    let project = this.props.project;
    return (
      <li style={{ marginLeft: "3vh" }} className="mb-2">
        <Flex justify="space-between">
        <Link
          to={{
            pathname: `/${project.id}`,
            project: project,
          }}
          key={project.id}
        >
          <div className="color-20">{project.name}</div>
        </Link>

          <div>
            <Popover>
              <PopoverTrigger>
                <Button bg="#FAFAFA" marginTop="-1.5vh" className="Link">
                  ...
                </Button>
              </PopoverTrigger>
              <PopoverContent>
                <PopoverArrow />
                <PopoverCloseButton className="Link"/>
                <PopoverBody>
                  <Flex direction="column" gap="2vh">
                    <div
                      onClick={() =>
                        this.setState({ isEditProjectPopup: true })
                      }
                      className="Link cursor"
                    >
                      Edit Project
                    </div>

                    <div onClick={() => this.setState({ isDeletePopup: true })} className="Link cursor">
                      Delete Project
                    </div>
                  </Flex>
                </PopoverBody>
              </PopoverContent>
            </Popover>
          </div>
        </Flex>

        {this.state.isEditProjectPopup && (
          <Modal show="true">
            <div className="fs-5 " style={{ fontWeight: "600" }}>
              Update Project
            </div>

            <Modal.Body>
              <div>Name</div>
              <input
                className="border"
                onChange={(e) =>
                  this.setState({
                    newProjectName: e.target.value,
                  })
                }
                defaultValue={this.state.newProjectName}
                value={this.state.newProjectName}
              ></input>
            </Modal.Body>
            <div className="modal-footer">
              <button
                type="button"
                onClick={() => this.setState({ isEditProjectPopup: false })}
                className="btn btn-secondary"
              >
                Cancel
              </button>

              <button
                type="submit"
                className="btn btn-danger"
                onClick={() => this.updateProject(project.id)}
              >
                Update
              </button>
            </div>
          </Modal>
        )}

        {this.state.isDeletePopup && (
          <Modal show="true">
            <div className="fs-5 " style={{ fontWeight: "600" }}>
              {" "}
              Delete Project
            </div>

            <Modal.Body>
              <div>
                Are you sure you want to delete{" "}
                <span style={{ fontWeight: "600" }}>{project.name}</span>
              </div>
            </Modal.Body>
            <div className="modal-footer">
              <button
                type="button"
                onClick={() => this.setState({ isDeletePopup: false })}
                className="btn btn-secondary"
              >
                {" "}
                Cancel
              </button>

              <button
                type="submit"
                className="btn btn-danger"
                onClick={() => this.deleteProject(project.id)}
              >
                Delete
              </button>
            </div>
          </Modal>
        )}
      </li>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    projects: state.projectsReducer.projects,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    deleteProject: (id) => dispatch(deleteProject(id)),
    updateProject: (id, newName) => dispatch(updateProject(id, newName)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Project);

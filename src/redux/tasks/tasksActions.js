import { GETALLTASKS, CREATETASK, DELETETASK, UPDATETASK } from "./tasksActionTypes";

export const getAllTasks = (tasks) => {
  return {
    type: GETALLTASKS,
    tasks,
  };
};

export const createTask = (id,newTask) => {
  return {
    type: CREATETASK,
    id,
    newTask,
  };
};

export const deleteTask = (id) => {
  return {
    type: DELETETASK,
    id,
  };
};

export const updateTask=(id,newName)=>{
  return {
    type : UPDATETASK,
    id,
    newName
  }
}